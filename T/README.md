# Anforderungen

## Inhaltsverszeichnis
- [1. Kapitel](#1-kapitel)
- [2. Kapitel](#2-kapitel)
- [Weitere Kapitel](#weitere-kapitel)

### Funktionstest
#### Mailserver
|Test|Erwartetes Ergebnis|Aktueller Status
|:--- |:---- |:---- 
|versandmöglichkeit Mails|Mails können verschickt werden|OK
|Automatische Mails|Sind Möglich (test Mails versendet)|OK
|Performance|Leistung des Servers reicht für einen normalen gebrauch des Dienstes|OK
|layout|Mails haben einen Bereff und Inhalt|OK
|Test|Tesumgebung ist vorhanden|NOK
|Test|Die aktuelle Version der SOftware wurde gründlich und ausreichend überprtüft vor dem veröffentlichen auf dem Hauptserver|OK
|Runbook| Runbook zum wiederaufbau der Umgebung ist vorhanden|OK
|Reaktionszeiten| Die reaktionszeiten sind gut im internen Netzwerk(nicht zu grosse Bilder etc.)|OK
|Erreichbarkeit Streamingserver |Streaming server ist Global erreichbar|Ok
|Upload|Mails können auf dem lokalen Computer geschrieben werden und das in den Server gezogen werden|OK




#### Webserver
|Test|Erwartetes Ergebnis|Aktueller Status
|:--- |:---- |:---- 
|Erreichbarkeit Webseite |Webseite ist im aktuellen Netzwerk erreichbar |OK
|Performance|Leistung des Servers reicht für einen normalen gebrauch des Dienstes|OK
|Bilder|Bilder sind sichtbar|OK
|CSS|CSS File wird erkannt und funktiioniert|OK
|Responsive|Die Websiete ist Responisve|OK
|Upload|Upload von neuem Content ist möglich|OK
|Test|Tesumgebung ist vorhanden|OK
|Test|Die aktuelle Version der Webseite wurde gründlich und ausreichend überprtüft vor dem veröffentlichen auf dem Webserver|OK
|Alternativ Texte|Alternativ Texte für die Bilder sind vorhanden|OK
|Reaktionszeiten| Die reaktionszeiten sind gut im internen Netzwerk(nicht zu grosse Bilder etc.)|OK



#### Streamingserver
|Test|Erwartetes Ergebnis|Aktueller Status
|:--- |:---- |:---- 
|Erreichbarkeit Streamingserver |Streaming server ist Global erreichbar|Ok
|Performance|Leistung des Servers reicht für einen normalen gebrauch des Dienstes|NOK
|Responsive|Die Websiete ist Responisve|OK
|Medien|Laufen flüssig|OK
|Medien|Werden erkannt von Plex, Titelbild, beschreibung und Cast werden angezeigt|OK
|Medien|Uploads neuer Medien möglich|OK
|Medien|es wird abgespeichert wo man gerade in einem Film ist und man kann beim nächsten mal einlogen dort Weiterschauen|OK
|Sprache|Plex ist auf die Reichtige Sprache|OK
|Features|Die neusten Features idn verfügbar|OK
|Account|man kann sich mit Google Accounts einlogen|OK

### Sicherheitstest
#### Streamingserver
|Test|Erwartetes Ergebnis|Aktueller Status
|:--- |:---- |:---- 
|Accounts|Nur berechtigte Accounts können sich einlogen|OK
|Accounts|Alle notwendigen Accounts sind hinzugefügt und können auf die Medien Zugreiffen|OK
|Backup|Dateien welche für das Betreiben des Servers gebraucht werden sind gesichert|OK
|Passwort|Passwort ist nicht älter als 30 Tage|OK
|Dienste udn Services|Dienste und Services welche nicht aktiv sind, sind deaktiviert|OK
|Server Accounts|Nur Christians Account ist aktiv und login fähig|OK
|Recovery|Dienst kann inerhalb einer Stunde auf neuer hardware wieder zum laufen gebracht werden und verrursacht so kein SLA breach|NOK
|Erreichbarkeit|Webinterface ist nur über https erreichbar|NOK
|Software|Software ist auf der neusten Version|OK
|ssh|Ssh ist für remotezugriff aktiviert|OK


#### Webserver
|Test|Erwartetes Ergebnis|Aktueller Status
|:--- |:---- |:---- 
|Accounts|Nur berechtigte Accounts können sich einlogen|OK
|zertifikat|Besitzt ein funktionierendes Zertifikat|OK
|Zertifikat|wird von den Browsern als Sicher angesehen|NOK
|Server Accounts|Nur Christians Account ist aktiv und login fähig|OK
|Dienste und Services|Dienste und Services welche nicht aktiv sind, sind deaktiviert|OK
|Passwort|Passwort ist nict älter als 30 Tage|OK
|Backup|Dateien welche für das Betreiben des Servers gebraucht werden sind gesichert|OK
|Recovery|Dienst kann inerhalb einer Stunde auf neuer hardware wieder zum laufen gebracht werden|OK
|Erreichbarkeit|Webinterface ist nur über https erreichbar|OK
|Software|Software ist auf der neusten Version|OK

#### Mailserver
|Test|Erwartetes Ergebnis|Aktueller Status
|:--- |:---- |:---- 
|Accounts|Nur berechtigte Accounts können sich einlogen|OK
|Backup|Dateien welche für das Betreiben des Servers gebraucht werden sind gesichert|OK
|Passwort|Passwort ist nict älter als 30 Tage|OK
|Dienste udn Services|Dienste und Services welche nicht aktiv sind, sind deaktiviert|OK
|Server Accounts|Nur Christians Account ist aktiv und login fähig|OK
|Recovery|Dienst kann inerhalb einer Stunde auf neuer hardware wieder zum laufen gebracht werden|NOK
|Software|Software ist auf der neusten Version|OK
|Spam|Mails landen nicht im Spam Ordner|OK
|Konfiguratuon|Es existiert ein Aktuelles Backup der Konfiguration|OK
|ssh|Ssh ist für remotezugriff aktiviert|OK



