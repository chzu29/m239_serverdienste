# Anforderungen

## Inhaltsverszeichnis
- [1. Kapitel](#1-kapitel)
- [2. Kapitel](#2-kapitel)
- [Weitere Kapitel](#weitere-kapitel)

### Zertifikat erstellen 
Als erstes erstellen wir eine Directory in der wir das Zertifikat und den Key speichern können. Dafür benutzen wir folgenden command: "mkdir /etc/nginx/ssl"
Das nacher auf diese Directory zugegriffen werden kann müssen wir noch die Rechte anpassen. Das machen wir mit: "chmod 700 /etc/nginx/ssl" Mit hilfe von OpenSSL wird das Zertifikat dann erstellt: "openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/example.key -out /etc/nginx/ssl/example.crt


### Conf-File bearbeiten 
Das Conf-File soll muss bearbeitet werden. Darin gibt man den Server an und den Redirect. Das Zertifikat und der Key werden hier integriert um eine https verbindung zu erstellen.  Am schluss soll es folgendermassen aussehen:


```
server {
    listen 80 default_server;
    server_name 10.211.55.9;
    return 301 https://$server_name$request_uri;

}

server {
    listen 443 ssl;
    server_name 10.211.55.9

    ssl_certificate /etc/nginx/ssl/example.crt;
    ssl_certificate_key /etc/nginx/ssl/example.key;

    location / {
        root /var/www/html
        index index.html index.htm index.nginx-debian.html
    }
}
```

Zum schluss wird noch mit dem comman: "nginx -s reload" aktualisiert. Danach kann man per Redirect eine Verbindung per https erreichen. 
