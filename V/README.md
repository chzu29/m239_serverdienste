# Anforderungen

## Inhaltsverszeichnis
- [1. Kapitel](### Sicherheitskonzept)
- [2. Kapitel](### Gefahren und Risiko )
- [3. kapitel](### Betriebskonzept)

### Sicherheitskonzept
In diesem Abschnitt wird beschrieben wie die Sicherheit unserer Applikationen gewährleistet wird.

#### Backups und Restore

##### Webseite

Bei Jedem Change an der Webseite wird ein Full Backup erstellt (zusätzlich zur Version). Diese Backups werden offline auf einem geografisch getrennten System gespeichert.

Die Backups werden 5 Versionen zurück gespeichert.

##### Mail Server

Von den Mails machen wir einmal im der Woche full backupt und einmal am Tag Inkrementell. 

Die Backups werden immer für einen Monat behalten.

##### Streamingserver (PLEX)

Beim Streaming Server werden die Filmdateien auf einer Netzwerkfreigabe gespeichert und lokal auf dem System.

Die Backups müssen nicht für eine bestimmte zeit aufbewahrt werden.

###### Restore

Bei einem Kriesenfall wird der offline Server ans Netzwerk angeschlossen und das Backup über eine interne Share auf den neuen Server geladen.
Das physische Backup darf nicht in die nähe des Servers gebracht werden
Wichtig, ein neuer Container muss verwendet werden.

- Die Webseite darf maximal eine Stunde nicht erreichbar sein.
- der Mailserver darf maximal 20 min nicht erreichbar sein.
- Der Streamingserver darf maximal 24h nicht erreichbar sein. (Das Backup wird hier nicht über die share zurückgeladen, es muss lokal eien kopie erstellt werden und dann das physische Backup zum Server gebracht werden.)

#### User

- Auf dem Container gibt es einen User, das Passwort wird von Christian Zurschmitten aufbewahrt, nach erfolgreichem Test werden die neuen Versionen von ihm hochgeladen.
- Das Passwort wird einmal im Monat geändert, es darf nicht aus "echten" Wörtern bestehen, muss mindestens einen Grossbuchstaben und ein Sonderzeichen haben.
- Neue Accounts müssen Bei Christian Zurschmitten beantragt werden.

#### Services 

- Alle unnötigen Services sind nicht installiert, Services (z.B. FTP) welche nicht die ganze Zeit gebraucht werden, müssen deaktiviert werden.
- Jedes halbes Jahr muss ein auszug der Services an Christian geschickt werden. dieser wird archiviert.

#### Ports

Nicht gebrauchte Ports werden geschlossen, auch hier wird einmal jedes halbe Jahr ein auszug der Firewall erstellt und an CHristian Zurschmitten gesendet.

### Gefahren und Risiko 

#### Gefahren

Jede Applikation hat ihren eigenen Container und der Webserver Container ist in einem anderen Subnet als die andere beiden Server. es besteht kein Rounting oder brdige zwischen den beiden Subnetzten. So entsteht eine DMZ.

Alle Maschinen werden auf dem gleichen physischen Gerät gehosted, das bringt die Gefahr mit sich, dass wenn der phsyische Server ein Problem hat sind alle drei Dienste weg.

#### Risiko
##### Risiko Analyse
|Schadensklasse|Finanzieller Schaden|Ausfall Kernprozesse|Reputationsschaden|Auswirkungen auf natürliche Personen
|:--- |:---- |:---- |:---- |:---- |
|Gering|< 5.000|Minimale Verzögerungen in den nachfolgenden Prozessen (bis zu 2 Stunden)|Vorfall ist nur internen Mitarbeitern bekannt. Keine medialen Auswirkungen|Nachteile (wirtschaftlich, gesellschaftlich) im geringen Umfang für die Person
|mittel|Zwischen 5.000 und 20.000|Führt zu einer Verzögerung von ca. einen Tag bei den nachfolgenden internen Prozessen|Regionale mediale Auswirkungen|Finanzieller Schaden (nicht existenzgefährdend)
|hoch|Zwischen 20.000 und 50.000|Führt zu einer Verzögerung von mehr als einen Tag bei den nachfolgenden internen Prozessen|Vorfall hat nationale Mediale Auswirkungen, negatives Images auch bei Stellenausschreibungen|Identitätsdiebstahl, Diskriminierung
|sehr hoch|> 50.000|Führt zu einer Verzögerung bei den geplanten Lieferzeiten; Kundentermine können nicht eingehalten werden|Vorfall hat internationale mediale Auswirkungen, Verlust von Kunden|Lebensgefahr, Existenzgefährdend

### Betriebskonzept
#### Datensicherung
|Modul|Aufgabe|Verantwortlicher Aufgabe|Ergebnis|Erstellung Ergebnis
|:--- |:---- |:---- |:---- |:---- |
|Datensicherung|erstellen und sichern von Backups|Fynn Merki|Report und Gesicherte Daten| Verantwortlicher
|Datensicherung|Verwalten der Backups|Fynn Merki|Inventarliste|Verantwortlicher
|Wiederherstellung|Wiederherstellung eines Backups|Fynn Merki|Report|Verantwortlicher
|Wiederherstellen|Testen eines Recoverys|Fynn Merki|gewissheit ob die Backups funktionieren|Verantwortlicher

#### OS und Services
|Modul|Aufgabe|Verantwortlicher Aufgabe|Ergebnis|Erstellung Ergebnis
|:--- |:---- |:---- |:---- |:---- |
|OS|Löschen von Usern|Christian Zurschmitten|Restriktives verwalten von Usern|Verantwortlicher
|Services|Überprüfen Welche Services gebraucht werden und wenn nicht Deaktivieren der Services|Christian Zurschmitten|Report|Verantwortlicher
|OS|Firewall anpassen|Christian Zurschmitten|Firewall so sicher restriktiv wie möglic zu halten|Verantwortlicher
|Patching|Systeme werden Regelmässig gepatcht|Fynn Merki|Server sind immer auf dem neusten Stand|Verantwortlicher

#### Webseite
|Modul|Aufgabe|Verantwortlicher Aufgabe|Ergebnis|Erstellung Ergebnis
|:--- |:---- |:---- |:---- |:---- |
|Content|erstellung neuer Texte/Bilder oder Videos|Fynn Merki|Bilder/Videos oder Texte|Verantwortlicher
|Upload|Neue Versionen der Webseite veröffentlichen|Christian Zurschmitten|Webseite mit neuem Content/Features|Verantwortlicher
|Upload|Vor dem Upload auf die echte Webseite in einer Testumgebung testen|Christian Zurschmitten|Gewissheit ob die Webseite funktioniert|Verantwortlicher
|Domain und Hosting|Server sind immer erreichbar und die Domain wird abbezahlt|Christian Zurschmitten|Server online und domain funktioniert|Verantwortlicher
|Patching|Systeme werden Regelmässig gepatcht|Fynn Merki|Server sind immer auf dem neusten Stand|Verantwortlicher

#### Mail-Server
|Modul|Aufgabe|Verantwortlicher Aufgabe|Ergebnis|Erstellung Ergebnis
|:--- |:---- |:---- |:---- |:---- |
|Verkehr|Mails können versendet werden|Fynn Merki|Mails können versendet werden|Verantwortlicher
|Verkehr|Mails können empfangen werden|Fynn Merki|Mails können empfangen werden|Verantwortlicher 
|Domain und Hosting|Die Server sind immer erreichbar und die Domain wird abbezahlt|Christian Zurschmitten|Server online und domain funktioniert|Verantwortlicher
|Patching|Systeme werden Regelmässig gepatcht|Fynn Merki|Server sind immer auf dem neusten Stand|Verantwortlicher

#### Streaming Server
|Modul|Aufgabe|Verantwortlicher Aufgabe|Ergebnis|Erstellung Ergebnis
|:--- |:---- |:---- |:---- |:---- |
|Content|erstellung/Beschaffung neuer Videos|Fynn Merki|Videos|Verantwortlicher
|Domain und Hosting|Die Server sind immer erreichbar und die Domain wird abbezahlt|Christian Zurschmitten|Server online und domain funktioniert|Verantwortlicher
|Patching|Systeme werden Regelmässig gepatcht|Fynn Merki|Server sind immer auf dem neusten Stand|Verantwortlicher

#### Administratives
|Modul|Aufgabe|Verantwortlicher Aufgabe|Ergebnis|Erstellung Ergebnis
|:--- |:---- |:---- |:---- |:---- |
|Dokumentation|Erstellung des Sicherhheitskonzepts|Fynn Merki|Sicherheitskonzept
|Dokumentation|Erstellung der Risikoanalyse|Fynn Merki|Risikoanalyse
|Dokumentation|Erstellung des Betriebskonzept|Fynn Merki|Betriebskonzept
|Dokumentation|Verwaltung der Dokumentationen|Fynn merki|Inventarliste





