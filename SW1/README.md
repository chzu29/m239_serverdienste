# Anforderungen

## Inhaltsverszeichnis
- [1. Kapitel](#1-kapitel)
- [2. Kapitel](#2-kapitel)
- [Weitere Kapitel](#weitere-kapitel)

### Webserver installieren/konfigurieren

Als Basis dient ein Linux Server auf dem nginx installiert wurde. Wir haben uns für nginx entschieden da wir es noch nie benutzt haben und die Reviews darüber sehr positiv waren. Nachdem die Installation abgeschlossen war haben wir direkt ein Conf-File erstellt in der Directory. "/etc/nginx/conf.d" mit dem Namen "www.linkme.com.conf".

```
server {
    listen 80 default_server;
    server_name www.linkme.com;

    location / {
        root /var/www/html;
        index index.html index.htm index.nginx-debian.html;
    }
}
```
### Webseite 

#### index.html

```
<!DOCTYPE html>

<html lang="en">

    <head>
        <link rel="stylesheet" href="style.css">
        <meta charset="UTF-8">  
    </head>

    <body>

        <header>
            <h1 class="linkme">linkme</h1>  
            <h1 class="automobile">automobile</h1>      
        </header>

        <div class="grid-container">

            <div class="ferrari458">
                <img src="img/ferrari458.jpeg"/>
            </div>
            <div class="cartext">
                Ferrari 458 Italia 4.5 V8<br>
                Automatisiertes Schaltgetriebe<br>
                Coupé<br>
                Benzin<br>
                566 PS
            </div>
            <div class="cartext">
                 3h = 450CHF<br>
                 6h = 700CHF<br>
                12h = 1300CHF<br>
                24h = 2200CHF
            </div>

            <div class="lambohuracan">
                <img src="img/lambohuracan.jpeg"/>
            </div>
            <div class="cartext">
                Lamborghini Huracan LP610-Spyder DCT<br>
                Automatisiertes Schaltgetriebe<br>
                Cabriolet<br>
                Benzin<br>
                610 PS
            </div>
            <div class="cartext">
                 3h = 450CHF<br>
                 6h = 700CHF<br>
                12h = 1300CHF<br>
                24h = 2200CHF
            </div>

            <div class="lambohuracan">
                <img src="img/porsche911.jpeg"/>
            </div>
            <div class="cartext">
                Porsche 911 Turbo S<br>
                Automatisiertes Schaltgetriebe<br>
                Coupé<br>
                Benzin<br>
                650 PS
            </div>
            <div class="cartext">
                 3h = 450CHF<br>
                 6h = 700CHF<br>
                12h = 1300CHF<br>
                24h = 2200CHF
            </div>

            <div class="mclarenp1">
                <img src="img/mclarenp1.jpeg"/>
            </div>
            <div class="cartext">
                McLaren P1<br>
                Automatisiertes Schaltgetriebe<br>
                Coupé<br>
                Benzin<br>
                916 PS
            </div>
            <div class="cartext">
                 3h = 450CHF<br>
                 6h = 700CHF<br>
                12h = 1300CHF<br>
                24h = 2200CHF
            </div>

          </div>

    </body>

</html>
```

#### style.css

```
body {
  background-color: black;
}

.linkme {
  color: white;
  text-align: center;
  margin-top: 3em;
  text-transform: uppercase;
  font-size: 50px;
  margin-right: 3em;
}

.automobile {
  color: white;
  text-align: center;
  margin-top: -0.8em;
  text-transform: uppercase;
  font-size: 50px;
  margin-left: 3em;
}

.grid-container {
  display: grid;
  grid-template-columns: auto auto auto;
  grid-gap: 5px;
  padding: 5px;
  margin-top: 15em;
}

.grid-container > div {
  text-align: center;
  padding: 5px 0;
  font-size: 30px;
}

img {
  height: 250px;
  border:solid 1px white;
}

.cartext {
  color: white;$
}

```

#### Bilder

Die Bilder sind wie gewohnt in einem seperaten Ordner "img" abgespeicher und werden so in html integriert. 

### Tests

Testfall|Ergebniss
|:--- |:---- |
|Service is auf Server sichtbar und aktiv| Mit dem command "sudo systemctl status nginx.service" ist sichbar das der Webserver aktiv ist und ohne probleme läuft|
|Per Browser auf den Server verbinden|Per IP Adresse ist der Webserver erreichbar|
|Wenn das HTML/CSS verändert wird, wird der Content im Browser verändert| Nach dem upload von unserem eigenen HTML/CSS ist der neue Content sichtbar im Browser|
|HTML/CSS können exportiert werden um auf einem anderen Webserver importiert werden| Webseite ist unverändert auf anderer VM|



