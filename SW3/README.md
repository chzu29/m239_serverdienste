# Anforderungen

## Inhaltsverszeichnis
- [1. Kapitel](#1-kapitel)
- [2. Kapitel](#2-kapitel)
- [Weitere Kapitel](#weitere-kapitel)

### Installation 
#### Package Download
![A test image](SW3/images/dok/plex1.PNG)


### Medien hochladen
![A test image](SW3/images/dok/plex2.PNG)


### Interne IP und port
http://192.168.1.139:32400/web/index.html
