# Know-How & Begriffe

## Inhaltsverszeichnis
- [1. Kapitel](#1-kapitel)
- [2. Kapitel](#2-kapitel)
- [Weitere Kapitel](#weitere-kapitel)

### Wie kann der selbe Webserver unterschiedliche Websites hosten? 

Ein Webserver kann mit hilfe von lokalen Domänen mehrere Websites hosten. Durch diese lokale Domäne können die User zur richtigen Directory geführt werden. 

Um lokale Domänen einzurichten muss die Datei "httpd-vhost.conf" bearbeitet werden. Um eine lokale Domäne hinzuzufügen muss ein weiterer Eintrag in das Dokument gemacht werden. Dieser kann wie folgt aussehen. Der

```
<VirtualHost *:80>
    Servername www.bespiel.com
    DocumentRoot c:/docker/www/beispiel.com
    <Directory "c:/docker/www/beispiel.com/">
        Options +Indexes +Includes +FollowSymLinks
        AllowOverride AllowOverride
        Require local
    </Directory>
</VirtualHost>
```

### Wie funktioniert ein DNS? Welche "Resource Records"-Typen gibt es? Wozu gibt es diese?

##### Wie funktioniert ein DNS?

DNS query = Anfrage vom Browser

DNS Resolution = Antwort mit der IP Adresse

Cache = Resolutions werden gespeichert von dem eigenen Computer und recursive Name Server um Zeit zu sparen. 

Ablauf von einer Abfrage:

1. Browser sendet eine Abfrage an den Server mit dem Domänen Name
2. Wird zuerst zu dem recursive Name Server geleitet (Vom Internetanbieter verwaltet)
3. Wenn der Name Server keinen passenden Eintrag hat geht die Anfrage weiter zu einem Der root Server (verwalten Top Level Domains —> .com)
4. Die Anfrage wird an den Top Level Domain Server gesendet und wird dann an einen authoritative Name Server gesendet.
5. Wenn die die passende resource gefunden wurde wird es an den recursive Name Server gesendet und von da aus zurück zum User.

- Recursive query
    - Der DNS muss mit dem angefragten “resource record” antworten
    - Wenn der “record” nicht gefunden wird muss der user eine Fehlermeldung bekommen
- Iterative query
    - Der DNS leiet die query an weitere DNS Server weiter
- Non-recursive query
    - Das sind queries welche schon resolved sind von einem DNS resolver. Heisst die resource die angefragt wurde isch schon bereit.
    - Stored in cache oder server ist schon authoritative

##### Welche "Resource Record"-Typen gibt es?

|Record Type|Aufgabe|
|:--- |:---- |
|A|Host Address|
|AAAA|IPv6 host address|
|ALIAS|Auto resolved alias|
|CNAME|Canonical name for an alias|
|MX|Mail Exchange|
|NS|Name Server|
|PTR|Pointer|
|SOA|Startof authority|
|SRV|location of service|
|TXT|Descriptive text|

##### Wozu gibt es diese?

Als Beispiel nehmen wir facebook.com, wenn man über z.B. Chrome auf die Webseite www.facebook.com zugreiffen will, sucht der der Client auf dem DNS nach dem A Record.
Wenn man aber z.B. ein E-Mail an Christian@facebook.com schreiben will sucht der server nach dem MX record, dieser ist zeigt nicht auf den Webserver wie der A-record aondern auf den den Mailserver.

Ein Resource Record wird immer in einem speziellen Format angegeben, bei einigen Records wird können auch noch weitere Atribute vor "rdata" eingefügt werden:

```
 ASCII-Format: <name> [<ttl>] [<class>] <type> <rdata> <length>

<name> - der Domänenname des Objekts, zu dem der Resource Record gehört (optional)
<ttl> - time to live (in Sekunden). Gültigkeit des Resource Records (optional)
<class>- Protokollgruppe zu der der Resource Record gehört (optional)
<type> - beschreibt den Typ des Resource Records
<rdata>- (resource data) Daten die den Resource Record näher beschreiben (zum Beispiel eine IP-Adresse für einen A-RR, oder einen Hostnamen für einen NS-RR)
<length> Länge der folgenden Daten
```
### Angaben über fremde Server

Files welche zugänglich sind:

- Key/Zertifikate wenn man mit einem Admin Account auf den Server eingelogt ist ist es nicht mehr schwer von dort die Privat keys oder Zertifikate zu stehlen.
- Auch die ganzen IP konfigurationen sind gefährlich, z.B. kann man so die IP einfach ersetzten 
- Auch das Regestry ist ohne Passwort erreichbar
- Passwörter welche im Klartext abgespeichert sind, können einfach mitgenommen werden
- Trusts und ssh connections können ausgenutzt werden
- Bei Physischen Servern kann z.B. das ILO passswort geändert werden und so kann der Server sogar Remote neu gestaged oder ganz zerstört werden.
- Log files aller art können gestohlen und ausgewertet werden, in diesen kann man vorallem andere Server, Usernamen und evtl Passwörter sehen.
- Services können gestartet und gestoppt werden.
- Lokale User können erstellt werden
- Softare und OS Versionen können herausgefunden werden.
- Software kann installiert werden
- Auf windows han man z.B. zugriff auf Diskpart --> Disks können whiped werden
- Informationen über die Firma können herausgefunden werden
- Eintrittpunkt ins Netzwerk --> VPN
- Firewall changes
- erstellen von Tasks/Scripts ausführen oder Software
- Backdoor einbauen

### Proxy

##### Was ist ein Proxy?

Proxy oder auch Proxy-Server ist ein Vermittler in einem Netzwerk. Er steht zwischen Client und den Netzwerkresourcen z.B. eine Webseite. Der Proxy ist ein Umweg den man nutzen kann um die Kommunikation zwischen Client und Netzwerkresource abzusichern, verschleiern oder zu beschleunigen. Wenn der Client eine Anfrage sendet wird diese zuerst an den Proxy gesendet und dieser schickt dann eine Anfrage an den richtigen Server mit der eigenen IP-Adresse und genau umgekehrt vom Server zum Proxy und dann zum Client zurück. 

Neben den Standart-Proxy (Forward-Proxys) gibt es aber auch noch Reverse-Proxys. Diese werden vorallem von Unternehmen genutzt die sich einen sicherheitstechnischen Vorteil verschaffen möchten.

##### Welche Funktionen übernimmt er?

- Beschleunigung
  - Der Cache des Proxy-Servers dient als zwischenspeicher, indem häufig verwendete Daten gespeichert sind. 
  - Somit kann schneller auf diese Daten zugegriffen werden
- Lastenverteilung und Bandbreiten einsparen
  - Er ist auch in der Lage die Bandbreite sowie die Lastenverteilung zu kontrollieren.
  -  Das macht der Proxy-Server indem er den Clients Bandbreiten zuweist und die Resourcen optimal zu Nutzen kommen. 
- Filterung
  - Ein Proxy-Server bietet dem Netzwerk-Administrator die Möglichkeit den lokalen Datenverkehr aktiv zu filtern.
  - Durch Einsatz von Whitelist und Blacklist können somit bestimmte Webseiten gesperrt werden. 
- Verschleierung
  - Der Client sendet die Anfrage an den Proxy nicht an den Ziel-Server
  - Der Proxy leitet die Anfrage mit seiner eigenen IP-Adresse weiter
  - So wird die IP-Adresse des Clients nicht veröffentlicht, so kann die herkunft teilweise verschleiert werden
- Forward-Proxy
  - Geräte in einem lokalen Netzwerk können von dem Internet getrennt werden und trotzdem darauf zugreifen
- Reverse-Proxy
  - Webserver lassen sich auch absichern z.B. genen Denial of Service Attacks
  - Die Clients haben keinen direkten Zugriff auf den Ziel-Server
  - Die Anfragen werden zuerst von dem Reverse-Proxy entgegen genommen und überprüft

### DMZ

##### Was ist eine DMZ

Eine Demilitarisierte Zone bezeichnet man ein Netzwerksegement, auf welches Zugriffe von Anonymen Gästen aus dem WAN erfolgen. meistens werden Dort Ressourcen gehostet welche sowohl fürs LAN sowie fürs WAN zugänglich sein sollten, wie z.B. websieten.
Die DMZ ist meistens durch eine Firewall vom Intranet getrennt und kann so nicht Viren oder 

##### Wozu dient Sie

Sie dient zur abgrenzung der öffentlichen Ressourcen, so können z.B. Viren nicht auf die anderen Systeme gelangen und wenn es Schwachstellen auf den system gibt können diese nicht auf den Rest des Netzwerkes angewandt werden.
Auch ist es gut für Schwachstellen welche im internenen Netzwerk bestehen z.B. offene Shares etc.

